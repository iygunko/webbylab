<?php

class FilmController extends Controller {

    /**
     * @return void
     */
    public function actionAdd () {
        if (isset($_POST["submit"])) {
            $options["Title"] = $_POST["Title"];
            $options["ReleaseYear"] = $_POST["ReleaseYear"];
            $options["Format"] = $_POST["Format"];
            $options["Stars"] = $_POST["Stars"];
            Film::addFilms($options);
        }
        Header("Location: /");
    }

    /**
     * @param int $id
     *
     * @return void
     */
    public function actionDeleteFilm ($id) {
        if (isset($_POST["submit"])) {
            Film::deleteFilmById($id);
        }
        Header("Location: /");
    }

    /**
     * @return bool
     */
    public function actionSearch () {
        if (isset($_POST["submit"])) {
            $param = $_POST["param"];
            $films = Film::getFilmsLike($param);
            if (!$films)
                $searchError = "Please enter Title or Star of the film";
        }
        require_once(ROOT_DIR."/views/index.php");

        return true;
    }

    /**
     * @return void
     */
    public function actionImport () {
        if($_FILES["userfile"]["type"] == "text/plain") {
            $file = $_FILES["userfile"]["tmp_name"];
            if (isset($_POST["submit"]))
                $fileContent = file($file);

            $filmsList = array();
            $j = 1;
            for ( $i = 0; $i <= count($fileContent) - 3; $i ++ ) {
                $filmsList[$j]["Title"] = substr($fileContent[$i], 7);
                $filmsList[$j]["ReleaseYear"] = substr($fileContent[$i + 1], 14);
                $filmsList[$j]["Format"] = substr($fileContent[$i + 2], 8);
                $filmsList[$j]["Stars"] = substr($fileContent[$i + 3], 7);
                Film::addFilms($filmsList[$j]);
                $j = $j + 1;
                $i = $i + 4;
            }
            Header("Location: /");
        }
    }
}