<?php


class IndexController extends Controller {

    /**
     * @return bool
     */
    public function actionIndex() {
        $films = array();
        $film = new Film();
        $films = $film->getFilms();
        require_once(ROOT_DIR."/views/index.php");

        return true;
    }

    /**
     * @return bool
     */
    public function actionAddNewFilm() {
        require_once(ROOT_DIR."/views/addNew.php");

        return true;
    }
}