<?php

class Db {

    /**
     * @return PDO $db
     */
    public static function getConnection () {
        $paramsPath = ROOT_DIR."/config/db_params.php";
        include ($paramsPath);
        try {
            $db = new PDO("mysql:host=".$host.";dbname=".$database, $user, $password);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db->exec("set names ".$charset);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        return $db;
    }
}