<?php

/**
 * @param string $class_name
 *
 * @return void
 */
function __autoload ($class_name) {
    $array_paths = array("/models/", "/apps/", "/controllers/",);
    foreach ( $array_paths as $path ) {
        $path = ROOT_DIR . $path . $class_name . ".php";
        if (is_file($path))
            include_once $path;

    }
}