<?php

class Film extends Model {

    const ORDER_BY_DEFAULT = "Title";

    /**
     * @param string $order_by
     *
     * @return array
     */
    public function getFilms ($order_by = self::ORDER_BY_DEFAULT) {
        $db = Db::getConnection();
        $stmt = $db->query("SELECT * from Films ORDER BY ".$order_by." ASC");
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $filmsList = array();
        $i = 0;

        while ( $row = $stmt->fetch() ) {
            $filmsList[$i]["id"] = $row["id"];
            $filmsList[$i]["Title"] = $row["Title"];
            $filmsList[$i]["ReleaseYear"] = $row["Release_Year"];
            $filmsList[$i]["Format"] = $row["Format"];
            $filmsList[$i]["Stars"] = $row["Stars"];
            $i ++;
        }

        return $filmsList;
    }

    /**
     * @param array $options
     *
     * @return bool
     */
    public static function addFilms ($options) {
        $db = Db::getConnection();
        $sql = "INSERT INTO Films "
            . "(Title, Release_Year, Format, Stars)"
            . "VALUES "
            . "(:Title, :ReleaseYear, :Format, :stars)";
        $result = $db->prepare($sql);
        $result->bindParam(":Title", $options["Title"], PDO::PARAM_STR);
        $result->bindParam(":ReleaseYear", $options["ReleaseYear"], PDO::PARAM_INT);
        $result->bindParam(":Format", $options["Format"], PDO::PARAM_STR);
        $result->bindParam(":stars", $options["Stars"], PDO::PARAM_STR);
        if ($result->execute())
            return true;

        return false;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public static function deleteFilmById ($id) {
        $db = Db::getConnection();
        $sql = "DELETE FROM Films WHERE id = :id";
        $result = $db->prepare($sql);
        $result->bindParam(":id", $id, PDO::PARAM_INT);

        return $result->execute();
    }

    /**
     * @param string $param
     *
     * @return array
     */
    public static function getFilmsLike ($param) {
        $param = "%".$param."%";
        $db = Db::getConnection();
        $query = "SELECT * FROM Films WHERE Title LIKE :param OR Stars LIKE :param";
        $query = $db->prepare($query);
        $query->bindParam(":param", $param, PDO::PARAM_STR);
        $query->execute();
        $result = $query->fetchAll();

        return $result;
    }

}