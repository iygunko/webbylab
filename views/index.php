<?php include_once(ROOT_DIR."/views/header.php"); ?>

<body>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index">Гарне Лого</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index">Головна</a>
                </li>
            </ul>
            <form class = "navbar-form navbar-right" action = "/Search" method = "POST">
                <div class="form-group">
                    <input type="text" name="param" class="form-control" placeholder="Search" required>
                </div>
                <button type = "submit" name = "submit" class="btn btn-default">Search</button>
            </form>
            <p style=" float: right; color: #ff0000; margin-top: 15px;"><?php if(isset($searchError)) echo $searchError; ?> </p>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<!-- Page Content -->
<div class="container">
    <!-- Jumbotron Header -->
    <header class="jumbotron hero-spacer">
        <h1>Привіт! </h1>
        <p>Тут ти можеш додати до системи новий фільм або оглянути те, що вже додано.</p>

        <!-- Trigger the modal with a button -->
        <a href="AddNewFilm" class="btn btn-info btn-lg">Додати новий фільм</a>
        <!-- Trigger the modal with a button -->
    </header>
    <hr>
    <!-- Title -->
    <div class="row"    >
        <div class="col-lg-12">
            <h3>Додані фільми:</h3>
        </div>
    </div>
    <!-- /.row -->
    <!-- Page Features -->
    <div class="row text-center">
        <?php foreach ($films as $filmItem): ?>
            <div class="col-md-4 col-sm-6 hero-feature" style="height: 330px; overflow: hidden; margin-top: 10px;">
                <div class="thumbnail">
                    <div class="caption">
                        <h3 style="margin-top: 0px;"><?php echo $filmItem["Title"]; ?></h3>
                        <p>Рік випуску: <?php echo $filmItem["ReleaseYear"]; ?></p>
                        <p>Формат: <?php echo $filmItem["Format"]; ?></p>
                        <p>Актори: <?php echo $filmItem["Stars"]; ?></p>
                        <form action = "/DeleteFilm/<?php echo $filmItem["id"];?>" method="POST">
                            <input type="hidden" value="">
                            <input type="submit" name="submit" class="btn btn-danger btn-md" value="Видалити">
                        </form>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <!-- /.row -->

<?php include_once(ROOT_DIR."/views/footer.php"); ?>