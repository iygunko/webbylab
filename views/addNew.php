<?php include_once(ROOT_DIR."/views/header.php"); ?>

<body>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index">Гарне Лого</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index">Головна</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<!-- Page Content -->
<div class="container">
    <!-- Jumbotron Header -->
    <header class="jumbotron hero-spacer">
        <h1>Привіт! </h1>
        <p>Тут ти можеш додати до системи новий фільм або оглянути те, що вже додано.</p>
        <form style = "float: right; " enctype="multipart/form-data" action="/Film/Import" method="POST"
            <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
            <h3>Імпортувати дані з файлу:</h3> <input name="userfile" type="file" />
            <input type = "submit" name = "submit" value = "Start Import" />
        </form>
        <form  name="add_new_task_form" id="add_new_film_form" action="/Film/Add" method="post">
            <p>Назва</p>
            <input name="Title" type="text" size="40" maxlength="40" required>
            <p>Рік випуску</p>
            <input name="ReleaseYear" type="number" min="1700" max="2017" maxlength="32" required>
            <p>Формат:
                <span>VHS </span><input class = "radio" type="radio" name="Format" value="VHS" checked>
                <span>DVD </span><input class = "radio" type="radio" name="Format" value="DVD">
                <span>Blu-Ray </span><input class = "radio" type="radio" name="Format" value="Blu-Ray">
            </p>
            <p>Зірки</p>
            <textarea name="Stars" cols="40" rows="5"  maxlength="150" required></textarea><br/><br/>
            <input name="submit" id="new_film_form_submit" class="btn btn-primary" type="submit"  value="Зберегти" class="btn">
        </form>
    </header>
    <hr>

<?php include_once(ROOT_DIR."/views/footer.php"); ?>